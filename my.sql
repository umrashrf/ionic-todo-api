# create database if not exists ionictodo character set utf8mb4 collate utf8mb4_unicode_ci;

# use ionictodo;

create table tasks (id int not null auto_increment,
                    info text not null,
                    due_date date not null,
                    assigned_to varchar(50) not null,
                    ts timestamp default current_timestamp(),
                    primary key (id)) ENGINE=InnoDB;

create table notes (id int not null auto_increment,
                    task_id int not null,
                    foreign key fk_task(task_id)
                        references tasks(id)
                        on update cascade
                        on delete restrict,
                    img text,
                    info text,
                    ts timestamp default current_timestamp(),
                    primary key (id)) ENGINE=InnoDB;

create table emails (id int not null auto_increment,
                     email_to varchar(50) not null,
                     subject varchar(50) not null,
                     body text not null,
                     ts timestamp default current_timestamp(),
                    primary key (id)) ENGINE=InnoDB;


create table task_emails (task_id int not null,
                          email_id int not null,
                          foreign key fk_task(task_id)
                            references tasks(id)
                            on update cascade
                            on delete restrict,
                          foreign key fk_email(email_id)
                            references emails(id)
                            on update cascade
                            on delete restrict) ENGINE=InnoDB;

create table note_emails (note_id int not null,
                          email_id int not null,
                          foreign key fk_note(note_id)
                            references notes(id)
                            on update cascade
                            on delete restrict,
                          foreign key fk_email(email_id)
                            references emails(id)
                            on update cascade
                            on delete restrict) ENGINE=InnoDB;

