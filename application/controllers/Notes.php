<?php
require_once(APPPATH . 'libraries/REST_Controller.php');
require_once(APPPATH . 'libraries/timeago.php');
require_once(APPPATH . 'libraries/email_manager.php');

class Notes extends REST_Controller
{
    public function index_get()
    {
        $data = array();
        $this->load->database();
        $query = $this->db->query('SELECT * FROM (SELECT * FROM notes n WHERE n.task_id = ' . $this->get('task_id') . ' ORDER BY ts DESC LIMIT '.$this->get('start').', '.$this->get('count').') nn ORDER BY nn.ts');
        foreach ($query->result() as $row)
        {
            $row->is_img = (trim($row->img) != '') ? 'true' : 'false';
            $row->img = $this->config->base_url() . $row->img;
            $row->since = time_elapsed_string($row->ts);
            array_push($data, $row);
        }
        $this->db->close();
        $this->response($data);
    }

    public function index_post()
    {
        $imgpath = null;
        foreach ($_FILES as $file)
        {
            $tmpfile = $file['tmp_name'];
            $filename = $file['name'];
            $output = explode('.', $filename);
            $filename = basename($tmpfile) . '.' . $output[count($output)-1];
            move_uploaded_file($tmpfile, "images/" . $filename);
            $imgpath = "images/" . $filename;
        }

        $task_id = $this->post('task_id');
        $note_info = $this->post('info') or '';

        $data = array(
            'task_id' => $task_id,
            'info' => $note_info,
        );

        if ($imgpath) {
            $data['img'] = $imgpath;
        }

        if (!$note_info) {
            $note_info = '<img src="' . $this->config->base_url() . $imgpath . '" width="800px" />';
        }

        $this->load->database();
        $query = $this->db->query("SELECT * FROM tasks WHERE id=$task_id");
        $task = $query->row();
        $result = $this->db->insert('notes', $data);
        $note_id = $this->db->insert_id();

        $task_due_date = $task->due_date;
        $task_info = $task->info;

        if ($result === true) {
            $subject = "New note created for your task";

            $body = "Task Id: $task_id<br>";
            $body .= "Task Due Date: $task_due_date<br>";
            $body .= "Task Description: $task_info<br>";
            $body .= "Note/photo id: $note_info<br>";

            $mailer = new EmailManager();
            $mailer->sendNewTaskEmail(
                $task->assigned_to,
                $subject,
                $body);

            $this->db->insert('emails', array(
                'email_to' => $task->assigned_to,
                'subject' => $subject,
                'body' => $body
            ));
            $email_id = $this->db->insert_id();

            $this->db->insert('note_emails', array(
                'note_id'=> $note_id,
                'email_id'=> $email_id
            ));
        }

        $this->db->close();
        $this->response($result);
    }
}
