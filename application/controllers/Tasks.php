<?php
require_once(APPPATH . 'libraries/REST_Controller.php');
require_once(APPPATH . 'libraries/email_manager.php');

class Tasks extends REST_Controller
{
    public function index_get()
    {
        $data = array();
        $this->load->database();
        $query = $this->db->query('SELECT *, (SELECT COUNT(id) FROM notes n WHERE n.task_id = t.id) notes_count FROM tasks t ORDER BY t.ts DESC;');
        foreach ($query->result() as $row)
        {
            array_push($data, $row);
        }
        $this->db->close();
        $this->response($data);
    }

    public function index_post()
    {
        $data = array(
            'info' => $this->post('info'),
            'due_date' => $this->post('due_date'),
            'assigned_to' => $this->post('assigned_to')
        );

        $this->load->database();
        $result = $this->db->insert('tasks', $data);
        $task_id = $this->db->insert_id();

        if ($result === true) {
            $mailer = new EmailManager();
            $mailer->sendNewTaskEmail(
                $this->post('assigned_to'),
                $this->post('due_date'),
                $this->post('info'));

            $this->db->insert('emails', array(
                'email_to' => $this->post('assigned_to'),
                'subject' => $this->post('due_date'),
                'body' => $this->post('info')
            ));
            $email_id = $this->db->insert_id();

            $this->db->insert('task_emails', array(
                'task_id'=> $task_id,
                'email_id'=> $email_id
            ));
        }

        $this->db->close();
        $this->response($result);
    }
}
