<?php
require_once('Mail.php');

class EmailManager
{
    public function sendNewTaskEmail($to, $subject, $body)
    {
        $from = 'noreply@propertyapps.co';

        $headers = array(
            'From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'MIME-Version' => 1,
            'Content-type' => 'text/html;charset=iso-8859-1'
        );

        $smtp = Mail::factory('smtp', array(
            'host' => 'smtp.sendgrid.net',
            'port' => 587,
            'auth' => true,
            'username' => 'apikey',
            'password' => getenv("SENDGRID_APIKEY")
        ));

        $mail = $smtp->send($to, $headers, $body);
    }
}
